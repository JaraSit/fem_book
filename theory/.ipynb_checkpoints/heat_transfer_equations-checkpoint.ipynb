{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "caa65816",
   "metadata": {},
   "source": [
    "# Rovnice vedení tepla\n",
    "\n",
    "Na úlohu jednorozměrného vedení tepla jsme už narazili. Než se pustíme do metody konečných prvků, pojďme si představit všechny její komponenty. Plné popsání našeho problému se nám pak bude hodit v následujících částech. V této části uvedeme převážně odvození pro jednorozměrný případ. Zobecnění pro 3D, které je přímým zobecněním uvedených vztahů, je uvedeno na konci.\n",
    "\n",
    "## Stacionární vedení tepla"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3acde7ea",
   "metadata": {},
   "source": [
    "V této úloze je cílem stanovit průběh skalární funkce teploty $T(x)$ v prostoru. Začněme odvozením základního bilančního vztahu, který lze získat z analýzy následujího jednorozměrného elementu vyňatého z námi vyšetřované tyče.\n",
    "\n",
    "```{image} figs/heat_element.png\n",
    ":class: bg-primary mb-1\n",
    ":width: 400px\n",
    ":align: center\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4dd50c38",
   "metadata": {},
   "source": [
    "Zde $Q(x)$ je hustota vnitřního zdroje a $q(x)$ je hustota tepelného toku. Tato veličina udává množství tepla, které projde za daný čas plochou tyče $A$. Jelikož uvažujeme prozatím stacionární stav, od časový rozměr zatím nebudeme uvažovat. Přímo z obrázku lze sestavit bilanční rovnici\n",
    "\n",
    "$$\n",
    "q(x)A + Q(x)A\\Delta x = q(x+\\Delta x)A,\n",
    "$$\n",
    "\n",
    "neboli teplo, které vteče do elementu, musí také vytéct. Vydělením délky elementu a limitním přechodem k nekonečně malému intervalu dostáváme základní rovnici vedení tepla\n",
    "\n",
    "$$\n",
    "\\frac{q(x+\\Delta x)-q(x)}{\\Delta x} = Q(x)\\quad \\Rightarrow_{\\Delta x\\rightarrow 0}\\quad \\frac{\\mathrm{d}q(x)}{\\mathrm{d} x} = Q(x).\n",
    "$$\n",
    "\n",
    "V naší rovnici se vyskytuje neznámá funkce tepelného toku $q$. Výhodnější je pracovat s teplotou a proto využijeme konstitutivní Fourierův zákon vedení tepla, který říká, že $q(x)=-\\lambda(x)T'(x)$. Parametr $\\lambda$ označuje součinitel tepelné vodivosti.  Po dosazení do předchozí rovnosti dostáváme hledanou diferenciální rovnici\n",
    "\n",
    "$$\n",
    "\\frac{\\mathrm{d}}{\\mathrm{d} x}\\left(\\lambda(x)\\frac{\\mathrm{d}T(x)}{\\mathrm{d}x}\\right) = Q(x).\n",
    "$$\n",
    "\n",
    "Pro kompaktnost zápisu budeme v 1D stacionárním případě používat čárku místo symbolu derivace, tedy rovnici lze ekvivalentně přepsat jako\n",
    "\n",
    "$$\n",
    "\\left(\\lambda(x)T'(x)\\right)' = Q(x).\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b781331c",
   "metadata": {},
   "source": [
    "Předpokládejme dále, že tato rovnice je v platnosti na nějaké vyšetřované doméně $\\Omega$. V našem případě jednorozměrného vedení tepla je dostačující uvažovat obyčejný interval $\\langle 0,L\\rangle$, kde $L$ je délka tyče. Dalším krokem je analýza okrajových podmínek na $\\Gamma=\\partial\\Omega$. Uvažujme, že v každém bodě hranice může být předepsána jen Dirichletova nebo jen Neunannova okrajová podmínka. Ta první z nich označuje vynucenou teplotu $T$. Oblast, kde se tak děje označme $\\Gamma_T$. Neumannova podmínka je předepsána na $\\Gamma_q$ a představuje vynucený tepelný tok. Platí tedy $\\Gamma = \\Gamma_T\\cup\\Gamma_q$. Uvažujme navíc 3 způsoby vynucení okrajového tepelného toku:\n",
    "* Předepsaná hodnota tepelného tok na $\\Gamma_{qp}$, vynucujeme tedy přímo hodnotu $q$.\n",
    "* Přestup tepla přes hranici na $\\Gamma_{qc}$, množství přeneseného tepla vyjadřujeme Newtonovým vztahem $q=\\alpha(T-T_0)$, kde $\\alpha$ je součinitel přestupu tepla a $T_0$ je vnější teplota.\n",
    "* Tepelný tok radiací na $\\Gamma_{qr}$, tento tepelný tok je dán výrazem $q=\\varepsilon\\sigma(T^4-T_\\infty^4)$, kde $\\varepsilon$ je pohltivost povrchu, $\\sigma$ je Stefan-Boltzmannova konstanta a $T_\\infty$ je teplota zářiče.\n",
    "\n",
    "Pro tyto hranice tedy platí $\\Gamma_q = \\Gamma_{qp} \\cup \\Gamma_{qc} \\cup \\Gamma_{qr}$. Náš systém je tedy plně popsán jako\n",
    "\n",
    "$$\n",
    "\\left(\\lambda(x)T'(x)\\right)' = Q(x),\\quad x\\in\\Omega \\\\\n",
    "T(x)=\\bar{T}(x),\\quad x\\in\\Gamma_T \\\\\n",
    "q(x)=\\bar{q}(x),\\quad x\\in\\Gamma_{qp} \\\\\n",
    "q(x)=\\alpha(T(x)-T_0),\\quad x\\in\\Gamma_{qc} \\\\\n",
    "q(x)=\\varepsilon\\sigma(T^4(x)-T_\\infty^4),\\quad x\\in\\Gamma_{qr}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1107ca9f",
   "metadata": {},
   "source": [
    "Výše jsou pruhem ($\\bar{q}$ a $\\bar{T}$) označeny předepsané veličiny. Pro odvození slabé formy vezmeme naši diferenciální rovnici a přenásobíme ji váhovou funkcí $\\delta T(x)$ a zintegrujeme přes celou zkoumanou doménu. Tedy\n",
    "\n",
    "$$\n",
    "\\int_\\Omega\\delta T(x)\\left(\\left(\\lambda(x)T'(x)\\right)' - Q(x)\\right)\\,\\mathrm{d}x=0,\\quad \\forall\\delta T, \\delta T=0 \\text{ na }\\Gamma_T.\n",
    "$$\n",
    "\n",
    "Aplikací Gaussova teorému (zobecněná integrace per partess) lze snížit nutnou regularitu hledané funkce $T(x)$. Navíc se do problému přirozeně vnoří všechny okrajové podmínky a po úpravě dostáváme výslednou slabou formu:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "635f5386",
   "metadata": {},
   "source": [
    "$$\n",
    "\\int_\\Omega\\delta T'(x)\\lambda(x) T'(x)\\,\\mathrm{d}x + \\int_{\\Gamma_{qc}}\\alpha\\delta T(x) T(x)\\,\\mathrm{d}x + \\int_{\\Gamma_{qr}}\\varepsilon\\sigma\\delta T(x) T^4(x)\\,\\mathrm{d}x = \\\\\n",
    "- \\int_{\\Gamma_{qp}}\\delta T(x)\\bar{q}\\,\\mathrm{d}x + \\int_{\\Gamma_{qc}}\\alpha\\delta T(x) T_0\\,\\mathrm{d}x + \\int_{\\Gamma_{qr}}\\varepsilon\\sigma\\delta T(x) T_\\infty^4(x)\\,\\mathrm{d}x + \\int_{\\Omega}\\delta T(x)Q(x)\\,\\mathrm{d}x, \\\\\n",
    "\\forall\\delta T, \\delta T=0 \\text{ na }\\Gamma_T.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "854bf783",
   "metadata": {},
   "source": [
    "## Nestacionární vedení tepla\n",
    "\n",
    "## Zobecnění do 3D"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4fdbc62",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
