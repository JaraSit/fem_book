{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2848667d",
   "metadata": {},
   "source": [
    "# Nedostatky silné formy\n",
    "\n",
    "Diferenciální rovnice jsou sice velmi silným nástrojem pro popis přirozených fyzikálních systémů, ale kladou až přílišný požadavek na spojitost řešení a jejich derivací. Přitom fyzikální systémy jsou ve své hluboké podstatě nespojitými jevy, zejména kvůli kvantovému charakteru. Na zjednodušené makroúrovni se sice může ukázat spojitý popis jako výhodný, ale občas se hodí od požadavku na spojitost lehce ustoupit. To lze demonstrovat hned na jednoduchém motivačním příkladu.\n",
    "\n",
    "Uvažujme rovnici vedení tepla pro jednoduchou tenkou tyč, která je dlouhá $L$ a pozice na tyči je charakterizována proměnnou $x$, tedy $x\\in\\langle 0,L\\rangle$. Pro funkci teploty $T(x)$ předpokládáme předepsané okrajové podmínky $T(0)=0, T(L)=1$. Diferenciální rovnice, která popisuje takový systém je rovna $-(p(x)T'(x))'=f(x)$, kde $p(x)$ je materiálový parametr a $f(x)$ je funkce vnitřních tepelných zdrojů. Budeme dále předpokládat, že levá polovina tyče je vyrobena z jiného materiálu než ta pravá přičemž $p(x)=1$ pro levou část zatímco $p(x)=2$ pro pravou část. Dále zjednodušeně uvažujme nulové vnitřní zdroje.\n",
    "Náš systém je tedy plně popsán následující okrajovou úlohou \n",
    "\n",
    "$$\n",
    "    -T''(x)=0, \\quad x\\in\\langle 0,L/2\\rangle,\\\\\n",
    "    -2T''(x)=0 \\quad x\\in( L/2,L\\rangle,\\\\\n",
    "    T(0)=0, \\quad T(L)=1.\n",
    "$$\n",
    "\n",
    "Jak uvidíme dále, nespojitost v bodě $L/2$ nám může přinášet jisté matematické obtíže, ale s odhlédnutím od rigoróznosti můžeme náš systém i přesto vyřešit, i když jen v inženýrském smyslu. Řešení na každé polovině lze získat přímou integrací, a jelikož je každá část z jiného materiálu, dostáváme po částech lineární řešení ve tvaru\n",
    "\n",
    "$$\n",
    "    T_1(x)&=C_1x+C_2, \\quad \\text{kde } x\\in\\langle 0,L/2\\rangle,\\\\\n",
    "    T_2(x)&=C_3x+C_4, \\quad \\text{kde } x\\in( L/2,L\\rangle.\n",
    "$$\n",
    "\n",
    "Pro jednodušší práci s nespojitostí jsme funkci řešení na každé části označili jiným symbolem. Řešení na levé části je označeno $T_1$, zatímco průběh teplot na druhé části jako $T_2$.\n",
    "Vyskytují se zde 4 neznámé parametry, které lze ale jednoduše a intuitivně zjistit. V první řadě musíme vynutit okrajové podmínky, čímž jsou dány hned 2 volné parametry\n",
    "\n",
    "$$\n",
    "    T_1(0)&=0 \\quad \\Rightarrow \\quad C_2=0,\\\\\n",
    "    T_2(L)&=1 \\quad \\Rightarrow \\quad C_4=1-C_3L.\n",
    "$$\n",
    "\n",
    "Další přirozenou podmínkou je spojitost funkce $T(x)$ v bodě $x=L/2$, tedy $T_1(L/2)=T_2(L/2)$. Veličinu $q=-p(x)T'(x)$ fyzikálně interpretujeme podle Fourierova zákona jako tok tepla. Tedy množství tepla, které projde průřezem tyče za jednotku času. Taková veličina musí být v celé tyči konstantní a to tedy zejména platí i v bodě $x=L/2$. Přírůstek toku v tomto bodě musí být nulový, matematicky zapsáno jako $q(L/2_-)=q(L/2_+)$.\n",
    "Z těchto dvou podmínek dostáváme i hodnoty posledních volných parametrů\n",
    "\n",
    "$$\n",
    "    T_1(L/2)&=T_2(L/2) \\quad \\Rightarrow \\quad C_1L/2=1-C_3L/2,\\\\\n",
    "    T_1'(L/2)&=2T_2'(L/2) \\quad \\Rightarrow \\quad C_1=2C_3.\n",
    "$$\n",
    "\n",
    "U druhé zmíněné podmínky nastává problém, protože funkce $T_1$ ani funkce $T_2$ nemají v bodě $L/2$ derivaci. Parametry $C_1$ a $C_3$ jsme získali uvažováním jednostranných derivací. \n",
    "Z právě zmíněných podmínek lze jednoduchými algebraickými úpravami získat výsledné řešení\n",
    "\n",
    "$$\n",
    "    %\\label{eq:var:initex_sol_1}\n",
    "    T(x)=\n",
    "    \\begin{cases}\n",
    "        -\\frac{4}{3L}x, \\quad \\text{kde } x\\in \\langle 0, L/2\\rangle,\\\\\n",
    "        \\frac{2}{3L}x + \\frac{1}{3}, \\quad \\text{kde } x\\in (L/2, L\\rangle.\n",
    "    \\end{cases}\n",
    "    %T_1(x) &= -\\frac{4}{3L}x, \\quad \\text{kde } x\\in \\langle 0, L/2\\rangle, \\\\\n",
    "    %T_2(x) &= \\frac{2}{3L}x + \\frac{1}{3}, \\quad \\text{kde } x\\in (L/2, L\\rangle.\n",
    "$$\n",
    "\n",
    "Toto řešení zaslouží drobný komentář.\n",
    "\n",
    "Námi nalezené řešení je problematické zejména v tom, že v matematickém smyslu nesplňuje původní diferenciální rovnici - v bodě $L/2$ neexistuje vlastní derivace. V inženýrském měřítku můžeme sice výsledek považovat za korektní, ale to nic nemění na tom, že výsledné řešení není dvakrát diferencovatelné a singularita v bodě $x/L$ nám neumožňuje řešení prohlásit za platné. Zde tato singularita nepřinesla tak velké obtíže, ale při použití numerických metod se může situace ještě více ztížit. \n",
    "Řešení splňující požadavek daný původní diferenciální rovnicí se nazývá tzv. **silné řešení**.\n",
    "Nicméně jak bylo ukázáno v příkladu výše, ne vždy je možné požadavky na spojitost derivací splnit. Je tedy přirozené z požadavků v jistém smyslu ustoupit. Zavádíme proto tzv. **slabé řešení**, které snižuje nároky na regularitu finálního řešení. Co je slabé řešení a jak ho odvodit bude ukázáno později."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29b517b0",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
