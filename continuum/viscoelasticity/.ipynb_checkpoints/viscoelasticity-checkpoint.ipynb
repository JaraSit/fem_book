{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2c544841",
   "metadata": {},
   "source": [
    "# Vazkopružnost"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13818fba",
   "metadata": {},
   "source": [
    "V následujícím budeme vyšetřovat numerickou odezvu vazkopružného materiálu. K této problematice existuje mnoho různých přístupů. Ukážeme pouze jeden z možných a to při aplikaci na vazkopružný 2D model s uvažováním setrvačných sil.\n",
    "\n",
    "Předpoklady všech představených modelů jsou následující:\n",
    "\n",
    "* Uvažujeme teorii malých deformací.\n",
    "* Předpokládáme, že Poissonovo číslo je konstantní. Pro porovnání s jinými materiálovými modely doporučujeme např.\\cite{Zemanova2017}\n",
    "* Materiálový bod je popsán pomocí zobecněného Maxwellova řetězce sestávajícího z pružiny s tuhostí $G_\\infty$ a několika Maxwellovými články. Každý článek je charakterizován pružinou s tuhostí $G_p$ a tlumičem s viskozitou $\\eta_p$. Články jsou indexovány parametrem $p\\in\\mathcal{P}$, kde $\\mathcal{P}=\\langle 0,P\\rangle\\subset\\mathbb{N}$.\n",
    "\n",
    "Pod těmito předpoklady je chování polymeru popsáno následující slabou formou\n",
    "\n",
    "$$\n",
    "    \\int_{\\Omega_{\\text{foil}}} \\delta{\\boldsymbol \\varepsilon}:\\left(G_\\infty\\mathbf{D}_\\nu:{\\boldsymbol \\varepsilon} + \\sum_{p\\in\\mathcal{P}}{\\boldsymbol \\sigma}_p\\right) \\ \\mathrm{d}\\Omega - \\delta\\mathcal{F}_\\mathrm{ext} = 0,\n",
    "$$\n",
    "\n",
    "doplněnou o $P$ obyčejných diferenciální rovnic, které popisují konstitutivní vývoj napětí\n",
    "\n",
    "$$\n",
    "    \\frac{\\dot{{\\boldsymbol \\sigma}}_p}{G_p} + \\frac{{\\boldsymbol \\sigma}_p}{\\eta_p} = \\mathbf{D}_\\nu:\\dot{{\\boldsymbol \\varepsilon}},\\ \\ \\ p\\in\\mathcal{P}.\n",
    "$$\n",
    "\n",
    "V této rovnici $\\delta \\mathcal{F}_\\mathrm{ext}$ značí virtuální práci vnějších sil pracujících na virtuálním posunutí $\\delta\\mathbf{u}$, tenzor malých deformací ${\\boldsymbol \\varepsilon}$ je symetrický gradient posunů ${\\boldsymbol \\varepsilon} = \\nabla_\\mathrm{s}\\mathbf{u}$. Bezrozměrný tenzor $\\mathbf{D}_\\nu$ souvisí s tenzorem tuhosti izotropního materiálu s uvažováním jednotkového smykového modulu. Napětí přenášené $p$-tým článkem je označen ${\\boldsymbol \\sigma}_p$.\n",
    "\n",
    "## Diskretizace\n",
    "Pro oba představené modely platí, že časovou doménu nejprve rozdělíme na celočíselný počet časových okamžiků $\\mathcal{T}=\\{ 0, \\ldots, t_\\mathrm{max}\\}$, \n",
    "kde $t_\\mathrm{max}$ je konečný čas naší výpočetní domény.\n",
    "\n",
    "### Zpětná Eulerova metoda\n",
    "Pro zpětnou Eulerovu metodu je časová derivace nahrazena zpětnou konečnou diferencí jako \n",
    "\n",
    "$$\n",
    "    \\dot{\\bullet}(t_{i+1})\\approx\\frac{\\bullet(t_{i+1}) - \\bullet(t_{i})}{\\Delta t_i}.\n",
    "$$\n",
    "\n",
    "Pro zkrácení zápisu, časový krok $\\Delta t_i$  je definován jako $t_{i+1} - t_i$ a explicitní závislost na času není explicitně zapisována, je nahrazena spodním indexem, tedy $\\bullet (t_i)\\equiv\\bullet_i$. \n",
    "\n",
    "Když použijeme tuto aproximační náhradu, poto je naše diferenciální rovnice pro Maxwellův článek vyčíslená v čase $t_{i+1}$ rovna\n",
    "\n",
    "$$\n",
    "    \\frac{\\boldsymbol{\\sigma}_{p,i+1} - \\boldsymbol{\\sigma}_{p,i}}{G_p\\Delta t_i} + \\frac{1}{\\eta_p}\\boldsymbol{\\sigma}_{p,i+1} = \\frac{1}{\\Delta t_i}\\mathbf{D}_\\nu:\\left(\\boldsymbol{\\varepsilon}_{i+1} - \\boldsymbol{\\varepsilon}_{i}\\right).\n",
    "$$\n",
    "\n",
    "Můžeme tedy explicitně vyjádřit $\\boldsymbol{\\sigma}_{i+1}$ jako\n",
    "\n",
    "$$\n",
    "\\boldsymbol{\\sigma}_{p,i+1} = \\frac{1}{\\zeta_{p,i}}\\left(\\mathbf{D}_\\nu:(\\boldsymbol{\\varepsilon}_{i+1} - \\boldsymbol{\\varepsilon}_{i})G_p + \\boldsymbol{\\sigma}_{p,i}\\right),\n",
    "$$\n",
    "\n",
    "kde $\\zeta_{p,i}=1+\\Delta t_i/\\tau_p$ je pomocný bezrozměrný parametr. Tenzor napětí může být přímo dosazen do rovnice rovnováhy, čímž získáme slabou formu pro $\\boldsymbol{\\varepsilon}_{i+1}$. Rovnice\n",
    "\n",
    "$$\n",
    "    \\int_{\\Omega_{\\mathrm{foil}}} \\delta\\boldsymbol{\\varepsilon}:\\left(G_\\infty +\\sum_{p\\in\\mathcal{P}} \\frac{G_p}{\\zeta_{p,i}}\\right)\\mathbf{D}_\\nu:\\boldsymbol{\\varepsilon}_{i+1}\\ \\mathrm{d}\\Omega \\nonumber \\\\\n",
    "    - \\int_{\\Omega_{\\mathrm{foil}}} \\delta\\boldsymbol{\\varepsilon}:\\sum_{p\\in\\mathcal{P}}\\frac{G_p}{\\zeta_{p,i}}\\mathbf{D}_\\nu:\\boldsymbol{\\varepsilon}_{i} \\ \\mathrm{d}\\Omega \\nonumber \\\\\n",
    "    + \\int_{\\Omega_{\\mathrm{foil}}}\n",
    "    \\delta\\boldsymbol{\\varepsilon}:\\sum_{p\\in\\mathcal{P}}\\frac{\\boldsymbol{\\sigma}_{p,i}}{\\zeta_{p,i}}\\ \\mathrm{d}\\Omega - \\delta\\mathcal{F}_\\mathrm{ext} = 0\n",
    "$$\n",
    "\n",
    "tedy řídí MKP analýzu pro získání $\\bm{\\varepsilon}_{i+1}$. Nakonec je napěťový tenzor aktualizován pomocí výše uvedené rovnice. Tato procedura je opakována pro všechny časové okamžiky.\n",
    "\n",
    "## Exponenciální algoritmus\n",
    "Pro odvození exponenciálního algoritmu uvažujeme lineární průběh deformace na intervalu $\\langle t_i, t_{i+1}\\rangle$, dále vizkózní napěťový tenzor je spočten přesným řešením přislušné ODR. Lineárně rozložená deformace indukuje konstantní rychlost deformace a řídící rovnice pro Maxwellův článek nabýv tvaru\n",
    "\n",
    "$$\n",
    "\\frac{\\dot{\\boldsymbol \\sigma}_{p}(t_i + s_i)}{G_p} + \\frac{{\\boldsymbol \\sigma}_{p}(t_i + s_i)}{\\eta_p}=\\frac{1}{\\Delta t_i}\\mathbf{D}_\\nu:\\left({\\boldsymbol \\varepsilon}_{i+1} - {\\boldsymbol \\varepsilon}_{i}\\right),\n",
    "$$\n",
    "\n",
    "kde $s_i$ je pomocný časový parametr začínající v $t_i$ a jdoucí dopředu v čase. Vzájemný vztah mezi originálním časem $t$ a proměnnou $s_i$ je $s_i=t-t_i$. \n",
    "Uvažujme dále počáteční podmínku ${\\boldsymbol \\sigma}_{p}(s_i=0)={\\boldsymbol \\sigma}_{p,i}$, potom je napětí vyčíslené v $s_i=\\Delta t_i$ tedy v $t=t_{i+1}$ rovno\n",
    "\n",
    "$$\n",
    "    {\\boldsymbol \\sigma}_{p,i+1}= {\\boldsymbol \\sigma}_{p,i}\\mathrm{e}^{-\\frac{\\Delta t_i}{\\tau_p}} \n",
    "$$\n",
    "\n",
    "$$\n",
    "    + \\frac{\\eta_p}{\\Delta t_i}\\left(1-\\mathrm{e}^{-\\frac{\\Delta t_i}{\\tau_p}}\\right)\\mathbf{D}_\\nu:({\\boldsymbol \\varepsilon}_{i+1} - {\\boldsymbol \\varepsilon}_i).\n",
    "$$\n",
    "\n",
    "Zpětná substituce do podmínky rovnováhy vyčíslené v čase $t_{i+1}$ dává konečně\n",
    "\n",
    "$$\n",
    "    \\int_{\\Omega_{\\mathrm{foil}}} \\delta{\\boldsymbol \\varepsilon}:\\left(G_\\infty +\\sum_{p\\in\\mathcal{P}} \\frac{\\eta_p}{\\Delta t_i}\\xi_{p,i}\\right)\\mathbf{D}_\\nu:{\\boldsymbol \\varepsilon}_{i+1}\\ \\mathrm{d}\\Omega \\nonumber \\\\\n",
    "    - \\int_{\\Omega_{\\mathrm{foil}}} \\delta{\\boldsymbol \\varepsilon}:\\sum_{p\\in\\mathcal{P}}\\frac{\\eta_p}{\\Delta t_i}\\xi_{p,i}\\mathbf{D}_\\nu:{\\boldsymbol \\varepsilon}_{i} \\ \\mathrm{d}\\Omega \\nonumber \\\\\n",
    "    + \\int_{\\Omega_{\\mathrm{foil}}}\n",
    "    \\delta{\\boldsymbol \\varepsilon}:\\sum_{p\\in\\mathcal{P}}\\mathrm{e}^{-\\frac{\\Delta t_i}{\\tau_p}}{\\boldsymbol \\sigma}_{p,i}\\ \\mathrm{d}\\Omega - \\delta\\mathcal{F}_\\mathrm{ext} = 0,\n",
    "$$\n",
    "\n",
    "kde je představena pomocná proměnná $\\xi_{p,i}=1-\\mathrm{e}^{-\\frac{\\Delta t_i}{\\tau_p}}$. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98b47336",
   "metadata": {},
   "source": [
    "## Dynamický řešič - Implementace [TOBE: Done]\n",
    "Velká část kódu je shodná se všemi předchozími příklady, proto je budeme komentovat jen stroze. Opět je v první řadě nutné načíst všechny knihovny a zvolit výpočetní parametry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "fe8ce3ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dolfinx as dfx\n",
    "import matplotlib.pyplot as plt\n",
    "import math\n",
    "from mpi4py import MPI\n",
    "import pyvista\n",
    "import ufl\n",
    "import numpy as np\n",
    "from petsc4py.PETSc import ScalarType\n",
    "import dolfinx.fem.petsc as dfp\n",
    "\n",
    "\n",
    "start_time = 0.0\n",
    "end_time = 30.0\n",
    "tau = 1.0e-1  # časový krok\n",
    "\n",
    "# Dimenze úlohy\n",
    "d = 2\n",
    "\n",
    "# Materiálové parametry, Gs, Ts jsou parametry Maxwellových článků\n",
    "n = 22\n",
    "Ginf = 682.18e3\n",
    "Gs = 1.0e3 * np.array(\n",
    "    [6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1,\n",
    "     111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])\n",
    "Ts = np.array(\n",
    "    [1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,\n",
    "     1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])\n",
    "eta = np.zeros(n)\n",
    "for k in range(0, n):\n",
    "    eta[k] = Ts[k]*Gs[k]\n",
    "\n",
    "rho = 1.0e6\n",
    "nu = 0.2\n",
    "\n",
    "# Lameho parametry pro G=1\n",
    "G_unit = 1.0\n",
    "lam, mu = (2 * G_unit * nu / (1 - 2 * nu), G_unit)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efdcb9da",
   "metadata": {},
   "source": [
    "Pro potřeby řešiče je vhodné nějaké materiálové konstanty spočítat dopředu."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "182ef1a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Other constants\n",
    "A = []\n",
    "B = []\n",
    "C = []\n",
    "sumA = 0.0\n",
    "sumB = 0.0\n",
    "sumC = 0.0\n",
    "\n",
    "for k in range(0, n):\n",
    "    A.append(math.exp(-Gs[k] / eta[k] * tau))\n",
    "    sumA += A[k]\n",
    "    B.append(-eta[k] * math.expm1(-Gs[k] / eta[k] * tau))\n",
    "    sumB += B[k]\n",
    "    C.append(0.5 * eta[k] * (tau + eta[k] / Gs[k] * math.expm1(-Gs[k] / eta[k] * tau)))\n",
    "    sumC += C[k]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b64a989",
   "metadata": {},
   "source": [
    "Opět vytvoříme výpočetní síť a potřebné funkční prostory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "91679ef8",
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh = dfx.mesh.create_unit_square(MPI.COMM_WORLD, 20, 20, dfx.mesh.CellType.quadrilateral)\n",
    "tdim = mesh.topology.dim\n",
    "fdim = tdim - 1\n",
    "\n",
    "w = dfx.fem.FunctionSpace(mesh, (\"P\", 1, (2,)))\n",
    "ten = dfx.fem.FunctionSpace(mesh, (\"P\", 1, (2,2)))\n",
    "\n",
    "dd_u = ufl.TrialFunction(w)\n",
    "v = ufl.TestFunction(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44e28b95",
   "metadata": {},
   "source": [
    "Při tvorbě okrajových podmínek využijeme stejného postupu jako v minulosti. Označíme spodní hranu číslem 11 a horní hranu číslem 22."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "7941fcc2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# --------------------\n",
    "# Boundary conditions\n",
    "# --------------------\n",
    "boundaries = [(11, lambda x: np.isclose(x[1], 0)),\n",
    "              (22, lambda x: np.isclose(x[1], 1))]\n",
    "\n",
    "def bottom(x):\n",
    "    return np.isclose(x[1], 0.0)\n",
    "\n",
    "facet_indices, facet_markers = [], []\n",
    "fdim = mesh.topology.dim - 1\n",
    "for (marker, locator) in boundaries:\n",
    "    facets = dfx.mesh.locate_entities(mesh, fdim, locator)\n",
    "    facet_indices.append(facets)\n",
    "    facet_markers.append(np.full_like(facets, marker))\n",
    "facet_indices = np.hstack(facet_indices).astype(np.int32)\n",
    "facet_markers = np.hstack(facet_markers).astype(np.int32)\n",
    "sorted_facets = np.argsort(facet_indices)\n",
    "facet_tag = dfx.mesh.meshtags(mesh, fdim, facet_indices[sorted_facets], facet_markers[sorted_facets])\n",
    "\n",
    "mesh.topology.create_connectivity(mesh.topology.dim-1, mesh.topology.dim)\n",
    "with dfx.io.XDMFFile(mesh.comm, \"facet_tags.xdmf\", \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "    xdmf.write_meshtags(facet_tag, mesh.geometry)\n",
    "\n",
    "ds = ufl.Measure(\"ds\", domain=mesh, subdomain_data=facet_tag)\n",
    "\n",
    "# Dirichlet boundary condition for bottom\n",
    "bottom_dofs = dfx.fem.locate_dofs_geometrical(w, bottom)\n",
    "u_bot = dfx.fem.Function(w)\n",
    "bcs = [dfx.fem.dirichletbc(u_bot, bottom_dofs)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "219bb849",
   "metadata": {},
   "source": [
    "Následně můžeme vytvořit všechny potřebné funkce a také soubory pro ukládání výsledků. Funkce <code>temp1</code>, <code>temp2</code> a <code>temp3</code> jsou jen pomocné funkce pro pomocné výpočty. Jedná se o funkce tenzorového charakteru pro update napěťových polí. Funkce <code>f_v</code> je sada funkcí reprezentující viskozní napětí."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "5a0ef7bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "u = dfx.fem.Function(w)\n",
    "d_u = dfx.fem.Function(w)\n",
    "dd_u_ = dfx.fem.Function(w)\n",
    "dd_u_old = dfx.fem.Function(w)\n",
    "f_v = [dfx.fem.Function(ten) for i in range(n)]\n",
    "\n",
    "t = start_time\n",
    "\n",
    "ff = dfx.fem.Constant(mesh, ScalarType([0.0, 1.0e6]))\n",
    "\n",
    "out_file = \"test2.xdmf\"\n",
    "with dfx.io.XDMFFile(mesh.comm, out_file, \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "    \n",
    "temp1 = dfx.fem.Function(ten)\n",
    "temp2 = dfx.fem.Function(ten)\n",
    "temp3 = dfx.fem.Function(ten)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb734cd7",
   "metadata": {},
   "source": [
    "Výpočet počáteční podmínky."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "fa4ca14a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Coefficient(FunctionSpace(Mesh(blocked element (Basix element (P, quadrilateral, 1, gll_warped, unset, False), (2,)), 1), blocked element (Basix element (P, quadrilateral, 1, gll_warped, unset, False), (2,))), 88)"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "LHS0 = rho*ufl.dot(v, dd_u)*ufl.dx\n",
    "RHS0 = ufl.dot(v, ff)*ds(1)\n",
    "problem = dfx.fem.petsc.LinearProblem(\n",
    "    LHS0, RHS0, u=dd_u_old, bcs=bcs, petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"}\n",
    ")\n",
    "problem.solve()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b814efbf",
   "metadata": {},
   "source": [
    "Následuje sestavení slabé formy. Ta je pouhým přepisem výše uvedených rovnic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "b6bad313",
   "metadata": {},
   "outputs": [],
   "source": [
    "def epsilon(a):\n",
    "    return ufl.sym(ufl.grad(a))\n",
    "\n",
    "\n",
    "def sigma(a):\n",
    "    return lam*ufl.tr(epsilon(a))*ufl.Identity(d) + 2.0*mu*epsilon(a)\n",
    "\n",
    "LHS = rho*ufl.dot(v, dd_u)*ufl.dx + ufl.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u))*ufl.dx\n",
    "RHS = -ufl.inner(epsilon(v), Ginf*sigma(u))*ufl.dx - ufl.inner(epsilon(v), (Ginf*tau + sumB)*sigma(d_u))*ufl.dx - \\\n",
    "        ufl.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u_old))*ufl.dx + ufl.dot(v, ff) * ds(22)\n",
    "RHS -= ufl.inner(epsilon(v), sum(A[k]*f_v[k] for k in range(0, n)))*ufl.dx"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "1510a0d0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Time instant  16.09999999999996\n"
     ]
    },
    {
     "ename": "TypeError",
     "evalue": "BoundingBoxTree.__init__() takes 2 positional arguments but 3 were given",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m/tmp/ipykernel_25036/1344453298.py\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m     28\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     29\u001b[0m     \u001b[0mx0\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0;36m0.5\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m0\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 30\u001b[0;31m     \u001b[0mtree\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mdfx\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgeometry\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mBoundingBoxTree\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mmesh\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     31\u001b[0m     \u001b[0mcells\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mgeometry\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcompute_first_entity_collision\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mtree\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mmesh\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mx0\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     32\u001b[0m     \u001b[0mu\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0meval\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mx0\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mcells\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mTypeError\u001b[0m: BoundingBoxTree.__init__() takes 2 positional arguments but 3 were given"
     ]
    }
   ],
   "source": [
    "# --------------------\n",
    "# Main load loop\n",
    "# --------------------\n",
    "while t <= end_time:\n",
    "    print(\"Time instant \", t)\n",
    "\n",
    "    problem = dfx.fem.petsc.LinearProblem(\n",
    "        LHS, RHS, u=dd_u_, bcs=bcs, petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"}\n",
    "    )\n",
    "    problem.solve()\n",
    "\n",
    "    # Update of rest quantities\n",
    "    temp1_exp = dfx.fem.Expression(sigma(d_u), ten.element.interpolation_points())\n",
    "    temp1.interpolate(temp1_exp)\n",
    "    temp2_exp = dfx.fem.Expression(sigma(dd_u_), ten.element.interpolation_points())\n",
    "    temp2.interpolate(temp2_exp)\n",
    "    temp3_exp = dfx.fem.Expression(sigma(dd_u_old), ten.element.interpolation_points())\n",
    "    temp3.interpolate(temp3_exp)\n",
    "    for k in range(0, n):\n",
    "        f_v[k].x.array[:] = A[k]*f_v[k].x.array + B[k]*temp1.x.array + C[k]*temp2.x.array + C[k]*temp3.x.array\n",
    "    u.x.array[:] = u.x.array + d_u.x.array*tau + 0.25*(dd_u_old.x.array + dd_u_.x.array)*tau*tau\n",
    "    d_u.x.array[:] = d_u.x.array + 0.5 * (dd_u_old.x.array + dd_u_.x.array) * tau\n",
    "    dd_u_old.x.array[:] = dd_u_.x.array\n",
    "\n",
    "    # Save solution to file (XDMF/HDF5)\n",
    "    #with dfx.io.XDMFFile(mesh.comm, out_file, \"a\") as xdmf:\n",
    "        #xdmf.write_function(u, t)\n",
    "    \n",
    "    x0 = [0.5, 1, 0]\n",
    "    tree = dfx.geometry.BoundingBoxTree(mesh, 1)\n",
    "    cells = geometry.compute_first_entity_collision(tree, mesh, x0)\n",
    "    u.eval(x0, cells)\n",
    "\n",
    "    # Increment of time\n",
    "    t = t + tau\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "045cc3bc",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e42d3cb2",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "007eab72",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "\n",
    "\n",
    "# --------------------\n",
    "# Classes and methods\n",
    "# --------------------\n",
    "def local_project(u_i, V_i):\n",
    "    ut = ufl.TrialFunction(V_i)\n",
    "    v_i = ufl.TestFunction(V_i)\n",
    "    uh = dfx.fem.Function(V_i)\n",
    "    problem = dfp.LinearProblem(\n",
    "        ufl.inner(ut, v_i) * ufl.dx, ufl.inner(u_i, v_i) * ufl.dx, u=uh, bcs=[],\n",
    "        petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"}\n",
    "    )\n",
    "    problem.solve()\n",
    "    return uh\n",
    "\n",
    "\n",
    "def top(x):\n",
    "    return np.isclose(x[2], 1.0)\n",
    "\n",
    "\n",
    "def bottom(x):\n",
    "    return np.isclose(x[2], 0.0)\n",
    "\n",
    "\n",
    "def epsilon(a):\n",
    "    return ufl.sym(ufl.grad(a))\n",
    "\n",
    "\n",
    "def sigma(a):\n",
    "    return lam*ufl.tr(epsilon(a))*ufl.Identity(d) + 2.0*mu*epsilon(a)\n",
    "\n",
    "\n",
    "def sigma_ten(a):\n",
    "    return local_project(lam*ufl.tr(ufl.grad(a))*ufl.Identity(d) + 2.0*mu*epsilon(a), ten)\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "# --------------------\n",
    "# Boundary conditions\n",
    "# --------------------\n",
    "boundaries = [(11, lambda x: np.isclose(x[2], 0)),\n",
    "              (22, lambda x: np.isclose(x[2], 1))]\n",
    "\n",
    "facet_indices, facet_markers = [], []\n",
    "fdim = mesh.topology.dim - 1\n",
    "for (marker, locator) in boundaries:\n",
    "    facets = dfx.mesh.locate_entities(mesh, fdim, locator)\n",
    "    facet_indices.append(facets)\n",
    "    facet_markers.append(np.full_like(facets, marker))\n",
    "facet_indices = np.hstack(facet_indices).astype(np.int32)\n",
    "facet_markers = np.hstack(facet_markers).astype(np.int32)\n",
    "sorted_facets = np.argsort(facet_indices)\n",
    "facet_tag = dfx.mesh.meshtags(mesh, fdim, facet_indices[sorted_facets], facet_markers[sorted_facets])\n",
    "\n",
    "mesh.topology.create_connectivity(mesh.topology.dim-1, mesh.topology.dim)\n",
    "with dfx.io.XDMFFile(mesh.comm, \"facet_tags.xdmf\", \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "    xdmf.write_meshtags(facet_tag, mesh.geometry)\n",
    "\n",
    "ds = ufl.Measure(\"ds\", domain=mesh, subdomain_data=facet_tag)\n",
    "\n",
    "# Dirichlet boundary condition for bottom\n",
    "bottom_dofs = dfx.fem.locate_dofs_geometrical(w, bottom)\n",
    "u_bot = dfx.fem.Function(w)\n",
    "bcs = [dfx.fem.dirichletbc(u_bot, bottom_dofs)]\n",
    "\n",
    "# --------------------\n",
    "# Initialization\n",
    "# --------------------\n",
    "u = dfx.fem.Function(w)\n",
    "d_u = dfx.fem.Function(w)\n",
    "dd_u_ = dfx.fem.Function(w)\n",
    "dd_u_old = dfx.fem.Function(w)\n",
    "f_v = [dfx.fem.Function(ten) for i in range(n)]\n",
    "\n",
    "t = start_time\n",
    "\n",
    "ff = dfx.fem.Constant(mesh, ScalarType([0.0, 1.0e6, 0.0]))\n",
    "\n",
    "# --------------------\n",
    "# XDMF output\n",
    "# --------------------\n",
    "# Create XDMF files for visualization output\n",
    "out_file = \"test2.xdmf\"\n",
    "with dfx.io.XDMFFile(mesh.comm, out_file, \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "\n",
    "# --------------------\n",
    "# Initial condition\n",
    "# --------------------\n",
    "LHS0 = rho*ufl.dot(v, dd_u)*ufl.dx\n",
    "RHS0 = ufl.dot(v, ff)*ds(1)\n",
    "problem = dfx.fem.petsc.LinearProblem(\n",
    "    LHS0, RHS0, u=dd_u_old, bcs=bcs, petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"}\n",
    ")\n",
    "problem.solve()\n",
    "\n",
    "# --------------------\n",
    "# Variational form\n",
    "# --------------------\n",
    "LHS = rho*ufl.dot(v, dd_u)*ufl.dx + ufl.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u))*ufl.dx\n",
    "RHS = -ufl.inner(epsilon(v), Ginf*sigma(u))*ufl.dx - ufl.inner(epsilon(v), (Ginf*tau + sumB)*sigma(d_u))*ufl.dx - \\\n",
    "        ufl.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u_old))*ufl.dx + ufl.dot(v, ff) * ds(22)\n",
    "RHS -= ufl.inner(epsilon(v), sum(A[k]*f_v[k] for k in range(0, n)))*ufl.dx\n",
    "\n",
    "temp1 = dfx.fem.Function(ten)\n",
    "temp2 = dfx.fem.Function(ten)\n",
    "temp3 = dfx.fem.Function(ten)\n",
    "\n",
    "# --------------------\n",
    "# Main load loop\n",
    "# --------------------\n",
    "while t <= end_time:\n",
    "    print(\"Time instant \", t)\n",
    "\n",
    "    problem = dfx.fem.petsc.LinearProblem(\n",
    "        LHS, RHS, u=dd_u_, bcs=bcs, petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"}\n",
    "    )\n",
    "    problem.solve()\n",
    "\n",
    "    # Update of rest quantities\n",
    "    temp1_exp = dfx.fem.Expression(sigma(d_u), ten.element.interpolation_points())\n",
    "    temp1.interpolate(temp1_exp)\n",
    "    temp2_exp = dfx.fem.Expression(sigma(dd_u_), ten.element.interpolation_points())\n",
    "    temp2.interpolate(temp2_exp)\n",
    "    temp3_exp = dfx.fem.Expression(sigma(dd_u_old), ten.element.interpolation_points())\n",
    "    temp3.interpolate(temp3_exp)\n",
    "    for k in range(0, n):\n",
    "        f_v[k].x.array[:] = A[k]*f_v[k].x.array + B[k]*temp1.x.array + C[k]*temp2.x.array + C[k]*temp3.x.array\n",
    "    u.x.array[:] = u.x.array + d_u.x.array*tau + 0.25*(dd_u_old.x.array + dd_u_.x.array)*tau*tau\n",
    "    d_u.x.array[:] = d_u.x.array + 0.5 * (dd_u_old.x.array + dd_u_.x.array) * tau\n",
    "    dd_u_old.x.array[:] = dd_u_.x.array\n",
    "\n",
    "    # Save solution to file (XDMF/HDF5)\n",
    "    with dfx.io.XDMFFile(mesh.comm, out_file, \"a\") as xdmf:\n",
    "        xdmf.write_function(u, t)\n",
    "\n",
    "    # Increment of time\n",
    "    t = t + tau\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
