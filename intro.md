# Úvod

Jedná se o sérii Jupyter-notebooků zaměřených na výpočet mechanických úloh metodou konečných prvků. K výpočtu touto metodou je použita zejména Python knihovna FEniCS a její novější nástupce FEniCSx. Část kódů je převzata a modifikována z https://bleyerj.github.io/comet-fenicsx/

:::{note}
Jedná se o výukovou pomůcku, která neprošla žádnou revizí ani jinou kontrolou. V textu lze tedy nalézt nedostatky nebo nekorektní výsledky. V případě takového nálezu, prosím o zpětnou vazbu.
:::

:::{tableofcontents}
:::
